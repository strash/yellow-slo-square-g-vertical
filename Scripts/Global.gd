extends Node


# енум имен сцен
enum VIEW_MAP {
	MENU, # экран меню
	SLOT_GAME, # экран игры слотово
	SLOT_LEVELS, # экран с выбором уровней (слотов/рулетки)
	ROULETTE_GAME, # экран игры рулетки
	AVIATOR_GAME, # экран игры авиатор
	INFO, # экран с текстовой информацией
}
# имена всех сцен
const VIEWS: PoolStringArray = PoolStringArray([
	"res://Scenes/Menu.tscn",
	"res://Scenes/Slot/SlotGame.tscn",
	"res://Scenes/Slot/Levels.tscn",
	"res://Scenes/Roulette/RouletteGame.tscn",
	"res://Scenes/Aviator/AviatorGame.tscn",
	"res://Scenes/Info/Info.tscn",
])

var REGEX: RegEx = RegEx.new()
var _regex_string: String = "(\\d)(?=(\\d\\d\\d)+([^\\d]|$))"
var _is_regex_ok: bool = false

const DELIMITER: String = ","


# BUILTINS -------------------------


func _init() -> void:
	_is_regex_ok = true if REGEX.compile(_regex_string) == OK else false


# METHODS -------------------------


# форматирование числа, проставление знаков между порядками
func format_number(number: float, delimiter: String = DELIMITER) -> String:
	var num_string: String = str(number)
	if _is_regex_ok:
		return REGEX.sub(num_string, "$1" + delimiter, true)
	else:
		return num_string


# добавление нулей для флоата
func add_zeroes(number: float, delimiter: String = ".") -> String:
	var decimal_index: int = step_decimals(number)
	match decimal_index:
		0:
			return delimiter + "00"
		1:
			return "0" if int(number * 100) % 10 == 0 else ""
		_:
			return ""


