extends Control


signal btn_back_pressed


var is_header_visible: bool = true setget set_header_visible
var is_score_visible: bool = true setget set_score_visible
var is_level_visible: bool = true setget set_level_visible
var is_btn_back_visible: bool = true setget set_btn_back_visible
var is_bg_visible: bool = true setget set_bg_visible


var HEADER_POS: float # header position.y
var BTN_BACK_POS: float # button back position.y
const GIFT_PRICE: int = 5000 # gift price
const SCORE_TWEEN_SPEED: float = 1.0


var SCORE: int = 300000 setget set_SCORE # общий счетчик
var LEVEL: int = 1 # уровень на шкале уровня
var LEVEL_SCORE: int = 0 setget set_LEVEL_SCORE # выигранные очки
var LEVEL_STEP: int = 300 # шаг уровня


const TWEEN_SPEED: float = 0.3
var _t: bool


# BUILTINS -------------------------


func _ready() -> void:
	set_level_progress(0)
	set_SCORE(SCORE)
	HEADER_POS = self.get_rect().position.y
	BTN_BACK_POS = ($BtnBack as Button).get_rect().position.y


# METHODS -------------------------


# изменение очков
func change_score(count: int, direction: int, with_level_progress: bool = true) -> void:
	if direction == 0: # увеличение
		if with_level_progress:
			set_level_progress(count)
		_t = ($Tween as Tween).interpolate_method(self, "set_SCORE", SCORE, SCORE + count, SCORE_TWEEN_SPEED)
	elif direction == 1: # уменьшение
		_t = ($Tween as Tween).interpolate_method(self, "set_SCORE", SCORE, SCORE - count, SCORE_TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка уровня
func set_level_progress(count: int) -> void:
	var level_progress: TextureProgress = $Level as TextureProgress
	var old_value: float = level_progress.value
	var new_value: float
	if LEVEL_SCORE + count > LEVEL * LEVEL_STEP:
		var first_half: int = LEVEL * LEVEL_STEP - LEVEL_SCORE # количество очков до полного уровня
		var second_half: int = count - first_half # количество оставшихся очков для следующего уровня
		var first_time: float = float(first_half) * SCORE_TWEEN_SPEED / count
		var second_time: float = second_half * SCORE_TWEEN_SPEED / count
		_t = ($Tween as Tween).interpolate_property(level_progress, "value", old_value, level_progress.max_value, first_time)
		_t = ($Tween as Tween).interpolate_method(self, "set_LEVEL_SCORE", LEVEL_SCORE, LEVEL_SCORE + first_half, first_time)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(first_time), "timeout")
		LEVEL += 1
		new_value = float(second_half) / (LEVEL * LEVEL_STEP) * level_progress.max_value
		_t = ($Tween as Tween).interpolate_property(level_progress, "value", 0.0, new_value, second_time)
		_t = ($Tween as Tween).interpolate_method(self, "set_LEVEL_SCORE", 0, second_half, second_time)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		new_value = float(LEVEL_SCORE + count) / (LEVEL * LEVEL_STEP) * level_progress.max_value
		_t = ($Tween as Tween).interpolate_property(level_progress, "value", old_value, new_value, SCORE_TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_method(self, "set_LEVEL_SCORE", LEVEL_SCORE, LEVEL_SCORE + count, SCORE_TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	($Level/IcLevel/LevelLabel as Label).text = "%s" % LEVEL


# SETGET -------------------------


# отображение хедера
func set_header_visible(state: bool) -> void:
	is_header_visible = state
	var pos_hide: float = -150.0
	if state:
		_t = ($Tween as Tween).interpolate_property(self, "rect_position:y", null, HEADER_POS, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property(self, "rect_position:y", null, pos_hide, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение денег
func set_score_visible(state: bool) -> void:
	is_score_visible = state
	if state:
		_t = ($Tween as Tween).interpolate_property($Score as Control, "modulate:a", null, 1.0, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($Score as Control, "modulate:a", null, 0.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение уровня
func set_level_visible(state: bool) -> void:
	is_level_visible = state
	if state:
		_t = ($Tween as Tween).interpolate_property($Level as TextureProgress, "modulate:a", null, 1.0, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($Level as TextureProgress, "modulate:a", null, 0.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение кнопки назад
func set_btn_back_visible(state: bool) -> void:
	is_btn_back_visible = state
	var pos_hide: float = -500.0
	if state:
		_t = ($Tween as Tween).interpolate_property($BtnBack as Button, "rect_position:y", null, BTN_BACK_POS, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($BtnBack as Button, "rect_position:y", null, BTN_BACK_POS + pos_hide, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение бэкграунда
func set_bg_visible(state: bool) -> void:
	is_bg_visible = state
	if state:
		_t = ($Tween as Tween).interpolate_property($BG as TextureRect, "modulate:a", null, 1.0, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($BG as TextureRect, "modulate:a", null, 0.0, TWEEN_SPEED)


func set_SCORE(num: int) -> void:
	SCORE = num
	if ($Score/Label as Label).is_visible_in_tree():
		($Score/Label as Label).text = Global.call("format_number", num as float)


func set_LEVEL_SCORE(num: int) -> void:
	LEVEL_SCORE = num
	if ($Level/LevelScoreLabel as Label).is_visible_in_tree():
		($Level/LevelScoreLabel as Label).text = "%s/%s" % [Global.call("format_number", num as float), Global.call("format_number", LEVEL * LEVEL_STEP)]


# SIGNALS -------------------------


func _on_BtnBack_pressed() -> void:
	emit_signal("btn_back_pressed")


func _on_BtnGift_pressed() -> void:
	change_score(GIFT_PRICE, 0, false)
	_t = ($Tween as Tween).interpolate_property($BtnGift as Button, "rect_position:y", ($BtnGift as Button).rect_position.y, -200.0, TWEEN_SPEED, Tween.TRANS_CUBIC)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


