extends Control


signal change_view_requested


# описание видимости блоков Header
const HEADER_SETUP: Dictionary = {
	"is_header_visible": true,
	"is_score_visible": true,
	"is_level_visible": true,
	"is_btn_back_visible": false,
	"is_bg_visible": true,
}

var payload: Dictionary = {} setget set_payload


# BUILTINS -------------------------


# METHODS -------------------------


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict


# SIGNALS -------------------------


func _on_Btn1_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 1 })


func _on_Btn2_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").AVIATOR_GAME, payload)


