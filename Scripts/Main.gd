extends Control


onready var STACK_CONTAINER: Control = $Stack as Control
export var START_SCREEN: int

# стэк вьюх состоящий из имен вьюх
var VIEW_STACK: PoolIntArray = PoolIntArray([])


# BUILTINS -------------------------


const TWEEN_SPEED: float = 0.3
var _t: bool


func _ready() -> void:
	randomize()
	add_view_to_stack(START_SCREEN, {})
	# подключение сигналов
	var _c: int = ($Header as Control).connect("btn_back_pressed", self, "_on_Header_btn_back_pressed")


# METHODS -------------------------


# добавление вьюхи в стэк и отображение ее в интерфейсе
func add_view_to_stack(view: int, payload: Dictionary) -> void:
	# подгружаем новую вьюху, вставляем в экран
	var next_view: Node
	next_view = (load(Global.get("VIEWS")[view]) as PackedScene).instance()
	# если во вьюхе есть сигналы переключения вьюх, то коннектим их
	var _c: int
	if next_view.has_signal("change_view_requested"):
		_c = next_view.connect("change_view_requested", self, "_on_View_change_view_requested")
	# управляем отображением элементов Header согласно настройкам вьюхи
	if next_view.get("HEADER_SETUP") != null:
		var gui_setup: Dictionary = next_view.get("HEADER_SETUP")
		for keys in gui_setup:
			($Header as Control).set(keys, gui_setup[keys])
	# добавляем в основную вьюху
	STACK_CONTAINER.add_child(next_view)
	# добавляем пейлоад во вьюху
	if next_view.get("payload") != null:
		next_view.set("payload", payload)
	VIEW_STACK.append(next_view.get_instance_id())
	# анимируем открытие вьюхи
	_t = ($Tween as Tween).interpolate_property(next_view, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# удаление вьюхи из стэка и удаление из интерфейса
func remove_view_from_stack() -> void:
	if not VIEW_STACK.empty():
		# дисконнектим текущую вьюху и мягко удаляем
		var view = instance_from_id(VIEW_STACK[VIEW_STACK.size() - 1])
		# управляем отображением элементов Header согласно настройкам вьюхи
		if not VIEW_STACK.empty() and VIEW_STACK.size() - 1 > 0:
			var prew_view = instance_from_id(VIEW_STACK[VIEW_STACK.size() - 2])
			if prew_view.get("HEADER_SETUP") != null:
				var gui_setup: Dictionary = prew_view.get("HEADER_SETUP")
				for keys in gui_setup:
					($Header as Control).set(keys, gui_setup[keys])
		# анимируем закрытие вьюхи
		_t = ($Tween as Tween).interpolate_property(view, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield($Tween, "tween_all_completed")
		# удаляем вьюху
		VIEW_STACK.remove(VIEW_STACK.size() - 1)
		if view.has_signal("change_view_requested"):
			view.disconnect("change_view_requested", self, "_on_View_change_view_requested")
		view.queue_free()


# SETGET -------------------------


# SIGNALS -------------------------


# вызывается, когда вьюха запрашивает открытие другой вьюхи
# то есть была нажата кнопка в этой вьюхе, которая должна открыть другую вьюху
func _on_View_change_view_requested(view: int, payload: Dictionary = {}) -> void:
	add_view_to_stack(view, payload)


# при нажатии кнопки назад в шапке
func _on_Header_btn_back_pressed() -> void:
	remove_view_from_stack()


