extends Control


const Column: PackedScene = preload("res://Scenes/Slot/Components/Column.tscn")


# описание видимости блоков Header
const HEADER_SETUP: Dictionary = {
	"is_header_visible": true,
	"is_score_visible": true,
	"is_level_visible": true,
	"is_btn_back_visible": true,
	"is_bg_visible": true,
}


enum { UP, DOWN }


var payload: Dictionary = {} setget set_payload
var textures: Array = [] # текстуры иконок всех уровней

var spinning: bool = false setget set_spinning # состояние спина
var auto_spinning: bool = false setget set_auto_spinning # состояние автоспина


const CHANSE_TO_WIN: int = 15 # шанс на победу. условно в процентах
const RATE: float = 2.0 # повышающий коэффициент при выигрыше

onready var FOOTER: Control = $Footer

const BGS: Array = [
	preload("res://Resources/Sprites/Slot/bg_lvl_1.png"),
]
const BOARDS: Array = [
	#null,
]
const SHADOWS: Array = [
	#[
	#	null, null,
	#],
]

const PROPS: = [
	{
		w = 200,
		h = 200,
		count_x = 3, # количество по горизонтали
		count_y = 3, # количество по вертикали
		offset_x = 50, # отступ между иконками
		offset_y = 10,
		variations = 4, # вариации иконок
		icons_offset_x = 0, # отступ иконок
		icons_offset_y = 30,
		board_bg_offset_x = 0, # отступ картинки доски
		board_bg_offset_y = 0,
		shadow_top_offset_y = 0, # отступ верхней тени
		shadow_bottom_offset_y = 0, # отступ нижней тени
	},
]


# BUILTINS -------------------------


func _ready() -> void:
	var _c: int
	_c = FOOTER.connect("btn_spin_pressed", self, "_on_Footer_btn_spin_pressed")
	_c = FOOTER.connect("btn_autospin_pressed", self, "_on_Footer_btn_autospin_pressed")
	# подгрузка текстур иконок
	for i in PROPS.size():
		textures.append([])
		for j in PROPS[i].variations:
			var icon: Texture = load("res://Resources/Sprites/Slot/ic_lvl_%s/%s.png" % [i + 1, j + 1])
			textures[i].append(icon)
	FOOTER.call_deferred("change_autospin_count")


# METHODS -------------------------


# загрузка уровня
func prepare_level(level: int) -> void:
	($Bg as TextureRect).texture = BGS[level - 1]
	($BoardBg as TextureRect).hide()
	if not BOARDS.empty() and BOARDS.size() >= level:
		($BoardBg as TextureRect).texture = BOARDS[level - 1]
		($BoardBg as TextureRect).set_size(Vector2.ZERO)
		($BoardBg as TextureRect).show()
	($ShadowTop as TextureRect).hide()
	($ShadowBottom as TextureRect).hide()
	if not SHADOWS.empty() and SHADOWS.size() >= level:
		if not SHADOWS[level - 1].empty():
			($ShadowTop as TextureRect).texture = SHADOWS[level - 1][0]
			($ShadowTop as TextureRect).set_size(Vector2.ZERO)
			($ShadowTop as TextureRect).show()
		if SHADOWS[level - 1].size() == 2:
			($ShadowBottom as TextureRect).texture = SHADOWS[level - 1][1]
			($ShadowBottom as TextureRect).set_size(Vector2.ZERO)
			($ShadowBottom as TextureRect).show()
	var prop: Dictionary = PROPS[level - 1]
	var board_w: float = prop.count_x * prop.w + prop.offset_x * (prop.count_x - 1)
	var board_h: float = prop.count_y * prop.h + prop.offset_y * (prop.count_y - 1)
	var offset: Vector2 = (self.get_rect().size - Vector2(board_w, board_h)) / 2 + Vector2(prop.icons_offset_x, prop.icons_offset_y)
	var offset_bg: Vector2 = (self.get_rect().size - ($BoardBg as TextureRect).get_rect().size) / 2 + Vector2(prop.board_bg_offset_x, prop.board_bg_offset_y)
	($Board as Control).set_size(Vector2(board_w, board_h))
	($Board as Control).set_position(offset)
	($BoardBg as TextureRect).set_position(offset_bg)
	var shadow_top: Vector2 = Vector2(($BoardBg as TextureRect).get_rect().size.x / 2 - ($ShadowTop as TextureRect).get_rect().size.x / 2 + offset_bg.x, offset_bg.y + prop.shadow_top_offset_y)
	var shadow_bottom: Vector2 = Vector2(($BoardBg as TextureRect).get_rect().size.x / 2 - ($ShadowBottom as TextureRect).get_rect().size.x / 2 + offset_bg.x, offset_bg.y + ($BoardBg as TextureRect).get_rect().size.y - ($ShadowBottom as TextureRect).get_rect().size.y + prop.shadow_bottom_offset_y)
	($ShadowTop as TextureRect).set_position(shadow_top)
	($ShadowBottom as TextureRect).set_position(shadow_bottom)
	for i in prop.count_x:
		var column: Control = Column.instance() as Control
		column.set("props", prop)
		column.set("textures", textures[level - 1])

		# чтобы в разные стороны крутилось
		if i % 2 == 1:

		# или
#		if i == 0 or i == prop.count_x - 1:
			column.set("direction", UP)
		else:
			column.set("direction", DOWN)

		# чтобы в одну сторону
		# column.set("direction", DOWN)

		column.set_position(Vector2((prop.w + prop.offset_x) * i, 0.0))
		column.set_size(Vector2(prop.w, board_h))
		($Board as Control).add_child(column)


# кручение слотов
func spin() -> void:
	if not spinning:
		set_spinning(true)
		var j: float = 0.0
		# НАЧАЛО КРУЧЕНИЯ
		for i in ($Board as Control).get_children():
			if not i is TextureRect:
				# задержка через одну колонку
				match j:
					1.0, 3.0:
						i.move_icons(0.2)
					2.0, 0.0, 4.0:
						i.move_icons(0.0)

				# задержка слева направо
				# i.move_icons(j / 7.0)

				j += 1.0
		yield(get_tree().create_timer(FOOTER.get("SPIN_TIME")), "timeout")
		j = 0.0
		# ОСТАНОВКА КРУЧЕНИЯ
		for i in ($Board as Control).get_children():
			if not i is TextureRect:
				# задержка через одну колонку
				match j:
					0.0, 4.0:
						i.stop_icons(0.0)
					1.0, 3.0:
						i.stop_icons(0.15)
					2.0:
						i.stop_icons(0.3)

				# задержка слева направо
				# i.stop_icons(j / 7.0)

				j += 1.0
		yield(get_tree().create_timer(1.5), "timeout")
		_spin_stop()
		calculate_score()


# вызов начала автоспина
func auto_spin() -> void:
	if not spinning and not auto_spinning:
		set_auto_spinning(true)
		spin()


# остановка спина. повторяется
func _spin_stop() -> void:
	set_spinning(false)
	if auto_spinning:
		yield(get_tree().create_timer(1.5), "timeout")
		_update_autospin_count()


# сигнал таймера паузы. перезапускает спин автоматически
func _update_autospin_count() -> void:
	# если меньше максимального количества спинов, то продолжаем крутить
	if FOOTER.get("AUTO_SPIN_MAX") > FOOTER.get("AUTO_SPIN_COUNT"):
		FOOTER.call_deferred("set_autospin_count", FOOTER.get("AUTO_SPIN_COUNT") + 1)
		spin()
	# если закончили, то останавливаем автоспины
	else:
		set_spinning(false)
		set_auto_spinning(false)
		FOOTER.call_deferred("set_autospin_count", 1)
	FOOTER.call_deferred("change_autospin_count")


# установка очков
func calculate_score() -> void:
	var HEADER: Control = get_node("/root/Main/Header")
	if randi() % 100 <= CHANSE_TO_WIN * FOOTER.get("BET") / 45 + CHANSE_TO_WIN:
		var count: int = floor(RATE * FOOTER.get("BET") + RATE * HEADER.get("LEVEL")) as int
		FOOTER.call_deferred("add_score", count)
		HEADER.call_deferred("change_score", count, 0)
	else:
		HEADER.call_deferred("change_score", FOOTER.get("BET"), 1)


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict
	prepare_level(payload.level)


# setget spinning
func set_spinning(status: bool) -> void:
	spinning = status
	FOOTER.set("spinning", status)


# setget auto_spinning
func set_auto_spinning(status: bool) -> void:
	auto_spinning = status
	FOOTER.set("auto_spinning", status)
	FOOTER.call_deferred("show_hide_autospin_progress", status)


# SIGNALS -------------------------


func _on_Footer_btn_spin_pressed() -> void:
	spin()


func _on_Footer_btn_autospin_pressed() -> void:
	auto_spin()


