extends Control


signal change_view_requested


# описание видимости блоков Header
const HEADER_SETUP: Dictionary = {
	"is_header_visible": true,
	"is_score_visible": false,
	"is_level_visible": false,
	"is_btn_back_visible": true,
	"is_bg_visible": true,
}


var view_h: float
var scrolling_container: MarginContainer
var scrolling_container_h: float
const TWEEN_SPEED: float = 0.15
var input_event_relative: float = 0.0


# BUILTINS -------------------------


func _ready() -> void:
	scrolling_container = $MarginScroll as MarginContainer
	if scrolling_container.is_visible_in_tree():
		view_h = get_viewport_rect().size.y
		scrolling_container_h = scrolling_container.get_rect().end.y


func _input(event: InputEvent) -> void:
	if scrolling_container.is_visible_in_tree():
		if scrolling_container_h > view_h:
			# вертикальный скроллинг плиток
			if event is InputEventScreenDrag:
				scrolling_container.rect_position.y += (event as InputEventScreenDrag).relative.y
				input_event_relative = (event as InputEventScreenDrag).relative.y
			# когда перестали скролить проверяем позицию плиток
			elif event is InputEventScreenTouch and not event.is_pressed():
				set_page()


# METHODS -------------------------


# выравнивание плиток, если они далеко проскролены
func set_page() -> void:
	var _t: bool
	if scrolling_container.get_rect().position.y > 0.0:
		_t = ($Tween as Tween).interpolate_property(scrolling_container, "rect_position:y", null, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(TWEEN_SPEED + 0.1), "timeout")
		input_event_relative = 0.0
	elif scrolling_container.get_rect().position.y + scrolling_container.get_rect().size.y < view_h:
		_t = ($Tween as Tween).interpolate_property(scrolling_container, "rect_position:y", null, view_h - scrolling_container.get_rect().size.y, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(TWEEN_SPEED + 0.1), "timeout")
		input_event_relative = 0.0


# SETGET -------------------------


# SIGNALS -------------------------


func _on_BtnLvl1_pressed() -> void:
	if input_event_relative == 0.0:
		emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 1 })


func _on_BtnLvl2_pressed() -> void:
	if input_event_relative == 0.0:
		emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 2 })


func _on_BtnLvl3_pressed() -> void:
	if input_event_relative == 0.0:
		emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 3 })


func _on_BtnLvl4_pressed() -> void:
	if input_event_relative == 0.0:
		emit_signal("change_view_requested", Global.get("VIEW_MAP").ROULETTE_GAME, { "level": 1 })


